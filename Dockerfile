FROM golang:1.12-alpine as builder
COPY . /outside-gopath
WORKDIR /outside-gopath
RUN CGO_ENABLED=0 GOOS=linux GOARCH=amd64 \
  go build -ldflags="-w -s" -mod vendor -o /go/bin/service

FROM scratch
COPY --from=builder /go/bin/service /go/bin/service
EXPOSE 9000
ENTRYPOINT [ "/go/bin/service" ]
