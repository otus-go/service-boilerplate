lint:
	golangci-lint run --enable-all ./...

test:
	go test -v -cover ./...

build:
	docker build -t service .

push:
	docker login -u $DOCKERHUB_LOGIN -p $DOCKERHUB_PASSWORD
	docker push $DOCKERHUB_LOGIN/service:latest
