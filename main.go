package main

import (
	"fmt"
	"log"
	"net/http"

	"github.com/joho/godotenv"
	"gitlab.com/otus-go/service-boilerplate/internal/config"
	"gitlab.com/otus-go/service-boilerplate/internal/service"
)

func main() {
	// ignore if .env file does not exist
	_ = godotenv.Load()
	conf := config.Config{}
	if err := conf.Load(); err != nil {
		log.Fatalln("Unable to load config ", err)
	}
	handler, err := service.NewService(conf.LogLevel.Level)
	if err != nil {
		log.Fatalln("Unable to init logger ", err)
	}
	addr := fmt.Sprintf(":%d", conf.Port)
	_ = http.ListenAndServe(addr, handler)
}
