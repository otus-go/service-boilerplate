# Каркас микросервиса
[![pipeline status](https://gitlab.com/otus-go/service-boilerplate/badges/master/pipeline.svg)](https://gitlab.com/otus-go/service-boilerplate/commits/master)
[![coverage report](https://gitlab.com/otus-go/service-boilerplate/badges/master/coverage.svg)](https://gitlab.com/otus-go/service-boilerplate/commits/master)

## Задание
Реализовать "каркас" микросервиса, считывающий конфиг из файла,
создающий логгер/логгеры с указанными уровнями детализации.

## Запуск
```bash
docker pull rbolkhovitin/service
docker run --rm -it -p 8080:8080 -e SERVICE_LOG_LEVEL=DEBUG rbolkhovitin/service
```

> образ очень маленький, буквально 3 МБ

## Что сделано

Реализован веб-сервис, который на любой запрос говорит "Hello World" :DDD
Конфигурацию берет из переменных окружения или файла .env.

Для чтения .env файла использован модуль
[godotenv](https://github.com/joho/godotenv)

Для преобразования переменных окружения в структуру
[envconfig](https://github.com/kelseyhightower/envconfig)

### Дополнительно
Добавлен CI-pipeline:
- прогон golangci-lint
- прогон тестов
- сборка docker образа и его пуш в docker hub
