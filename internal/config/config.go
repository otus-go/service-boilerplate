package config

import (
	"github.com/kelseyhightower/envconfig"
	"go.uber.org/zap/zapcore"
)

type LogLevelDecoder struct {
	zapcore.Level
}

// Decode - custom Decoder for LogLevel value
func (l *LogLevelDecoder) Decode(value string) error {
	return l.UnmarshalText([]byte(value))
}

type Config struct {
	Port     int             `default:"8080"`
	LogLevel LogLevelDecoder `default:"info" split_words:"true"`
}

func (c *Config) Load() error {
	return envconfig.Process("service", c)
}
