package service

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"strings"
	"testing"
	"time"

	"go.uber.org/zap/zapcore"
)

func TestDummy(t *testing.T) {
	handler, _ := NewService(zapcore.InfoLevel)
	server := httptest.NewServer(handler)
	client := http.Client{Timeout: 1 * time.Second}
	resp, _ := client.Get(server.URL)
	body, _ := ioutil.ReadAll(resp.Body)
	defer resp.Body.Close()
	if !strings.Contains(string(body), "Hello World") {
		t.Error("Unexpected Response")
	}
}
