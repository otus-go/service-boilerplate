package service

import (
	"net/http"

	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

type Service struct {
	Logger *zap.SugaredLogger
}

func (s Service) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	s.Logger.Debugw("handling request", "key", "value")
	_, _ = w.Write([]byte("Hello World\n"))
}

func NewService(level zapcore.Level) (*Service, error) {
	zapConfig := zap.NewProductionConfig()
	zapConfig.Level.SetLevel(level)
	zapLogger, err := zapConfig.Build()
	if err != nil {
		return nil, err
	}
	return &Service{Logger: zapLogger.Sugar()}, nil

}
